# PLEDGER Application Profiler

#### General Project Description

The PLEDGER Application Profiler is used to provide helpful information to the DSS. It is a Node-RED based component that creates execution profiles for containerized services. The profiles created, correspond to a set of already executed benchmark profiles. After the completion of the profiling process it communicates with the ConfService and tags the running service with the name of the benchmark that the profiler matched. The Application Profiler is controlled through a REST API and can be triggered by sending Kafka messages.

#### Software Requirements
| Prerequisites | Version |
| ------ | ------ |
| Node-Red | v1.3.5 |
| MongoDB | v4.4.13 |
| tiny-weka |

#### Node-Red Pallete 
| Nodes | Version |
| ------ | ------ |
| node-red | 1.3.5 |
| node-red-contrib-kafkajs | 0.0.7 |
| node-red-contrib-simple-message-queue | 0.2.8 |
| node-red-contrib-uuid | 0.0.4 |
| node-red-node-mongodb | 0.2.5 |


## Instructions for each flow
### Docker Benchmark Profile Exctractor

This flow is used to exctract a profile from an application running in a Docker container.
In order to start the profiling process, send a POST request to `/monitor2`. The body of the request must be in JSON format as follows:
```
{
    "cid": id of the container that contains the application we want to profile
    "bname": a unique name tha identifies the application we want to profile
}
 ```
 Everything that is calculated will be stored in a MongoDB database that must be set in the database nodes. These nodes are `Store Stats`, `Store Final Features` and `Find Features`. Aditionally, in these nodes the appropriate collections where the metrics will be stored must be set as follows:
 
| Node | Collection description that must be set|
| ------ | ------ |
| `Store Stats` | All recorded raw metrics and metrics that are produced |
| `Store Final Features` | Same as `Store Stats` |
| `Find Features` | All produced features that can be used for profile creation |
 
Lastly, in the `Mean Values Query` node, in the `$merge` field the same collection as in `Find Features` must be set.
 

The calculated profile will also be storred in csv format in a specified file. This path is must be set in the `Save Results` node.

### Application Profile Exctractor

This flow is used to exctract a profile from an application running in a Docker container and trigger the prediction operation which will decide the label of our model that best represents the application profile .
In order to start the profiling process, send a POST request to `/profile`. The body of the request must be in JSON format as follows:
```
{
    "add": ip address of the machine on which the container that runs the application is deployed
    "port": port of the machine on which the docker engine listens
    "cid": id of the container that contains the application we want to profile
    "bname": a unique name tha identifies the application we want to profile
    "uid": the user id of the application
    "aid": the application id
    "workload": the application workload level. It must be set to Low, Normal, High or Default.
    "type": the application type. It must be set to Default, Processing, IO, Data, Graphics, Network, Multithreading, Render, Compress, System or Memory
    "times": the number of times we want to profile the application
}
 ```
 
If the profiling is set to be done multiple times, a new profile will be created for every time, with an ascending number appended to the unique id tha is produced and stored to identify the specific profile. 


 Everything that is calculated will be stored in a MongoDB database that must be set in the database nodes. These nodes are `Store Stats`, `Store Final Features` and `Find Features`. Aditionally, in these nodes the appropriate collections where the metrics will be stored must be set as follows:
 
| Node | Collection description that must be set|
| ------ | ------ |
| `Store Stats` | All recorded raw metrics and metrics that are produced |
| `Store Final Features` | Same as `Store Stats` |
| `Find Features` | All produced features that can be used for profile creation |
 
Lastly, in the `Mean Values Query` node, in the `$merge` field the same collection as in `Find Features` must be set.

The calculated profile will also be stored in csv format in a specified file. This path is must be set in the `Save Results` node.

Another csv file will be created which will be used as input to the predictions model. It's path must be set in the `Create input csv` node.

### Prometheus Benchmark Profile Extractor

This flow is used to exctract a profile from an application running in a container deployed in a Kubernetes Cluster, the metrics of which are stored in a Prometheus Monitoring System database.
In order to start the profiling process, send a POST request to `/monitorProm`. The body of the request must be in JSON format as follows:
```
{
    "add": ip address of the prometheus database in which the metrics are stored
    "port": port that prometheus listens to
    "cid": id of the container that contains the application we want to profile
    "bname": a unique name tha identifies the application we want to profile
}
 ```
 Everything that is calculated will be stored in a MongoDB database named `Prom_Results`

The calculated profile will also be stored in csv format in a specified file. This path is must be set in the `Save Results` node.

### Prometheus Application Profile Extractor

This flow is used to exctract a profile from an application running in a container deployed in a Kubernetes Cluster, the metrics of which are stored in a Prometheus Monitoring System database and trigger the prediction operation which will decide the label of our model that best represents the application profile.
In order to start the profiling process, send a POST request to `/profileProm`. The body of the request must be in JSON format as follows:
```
{
    "cid": id of the container that contains the application we want to profile (pod id if it contains one container only)
    "endpoint": endpoint of the prometheus database in which the metrics are stored
    "times": "times": the number of times we want to profile the application
    "bname": a unique name tha identifies the application we want to profile
    "type": the application type. It must be set to Default, Processing, IO, Data, Graphics, Network, Multithreading, Render, Compress, System or Memory
}
 ```
Nothing will be stored as we just want a prediction and the information we need is already stored in the Prometheus database

A csv file will be created which will be used as input to the predictions model. It's path must be set in the `Create PromIn.csv` node.


### Random Forest Weka Trainer

This flow is used to train new predictions models. Different paths must be set before being able to create the models. The first one is the path to tiny-weka jar file, which contains the java classes needed for the training and prediction operations. This path must be set in both `Transform input to arff` and `Create * Model` nodes. The second one is the path to the csv file that will be used in order to train the model, this can be set in the  `Transform input to arff` node. The last one is the path that we want the resulting model to be saved. This path must be set in the  `Create * Model` nodes.

### Random Forest Weka Classifier

This flow is used to predict the benchmark that best represents an input profile for a random containerised application. Like in the previous flow, appropriate paths must be set in the `Transform input to arff` and `Predict and export csv` nodes. The produced prediction file's path must also be set in the `Read Predictions` node. Apart from the file, the prediction will also be printed in the debug console of node-red as well as in the MongoDB database collection that must be set in the 'Store Stats' node. After the prediction the Kafka flow operations will be triggered which will update the corresponding service's profile in the ConfService

### Kafka

This flow is the bridge for communication between tha app-profiler and the ConfService DSS through Kafka. It listens for new deployment messages and when the profile is calculated and predicted the ConfService is informed. The only thing that must be configured are the paths to the trustore and keystore files in the properties tab of `kafkajs-consumer` and `kafkajs-producer` nodes. 
### Request Handler

This flow enables parallel profiling, which means that multiple profiling requests can be served at the same time. In the node `Create Replicated Flow`, the flow that we want to be able to replicate  itself must be hardcoded in json format in the `profiler` variable. Also, in the node `Profiler Request` the corresponding endpoint for the flow we want to replicate must be set in the URL field. After this is done, in order to trigger the profiling one can send a profiling request to the `/par_profile` endpoint the exact same way he would send the request to the original endpoint. 



### Profiler ui

Every profiling procedure can also be configured and triggered through a correspdonding user interface. This interface by default can be found at `localhost:1880/ui`.

![Profiler Menu](screens/Menu.png)
![Docker Benchmark Profile Exctractor](screens/Docker_Benchmark_Profile_Exctractor.png)
![Application Profile Exctractor](screens/Application_Profile_Exctractor.png)
![Prometheus Benchmark Profile Exctractor](screens/Prometheus_Benchmark_Profile_Exctractor.png)
![Prometheus Application Profile Exctractor](screens/Prometheus_Application_Profile_Exctractor.png)


# Schema-Data Model

```{id, 
 avg_tasks,
 avg_memory,
 avg_rx_bytes,
 avg_rx_packets,
 avg_tx_bytes,
 avg_tx_packets,
 avg_io_read,
 avg_io_write,
 avg_delta_tasks,
 avg_delta_cpu,
 avg_delta_memory,
 avg_delta_rx_bytes,
 avg_delta_rx_packets,
 avg_delta_tx_bytes,
 avg_delta_tx_packets,
 avg_delta_io_read,
 avg_delta_io_write,
 rx_bytes,
 rx_packets,
 tx_bytes,
 tx_packets,
 io_read,
 io_write
 geo_avg_tasks,
 geo_avg_memory,
 geo_avg_net_rx_bytes,
 geo_avg_net_rx_packets,
 geo_avg_net_tx_bytes,
 geo_avg_net_tx_packets,
 geo_avg_io_read,
 geo_avg_io_write,
 geo_avg_delta_cpu,
 geo_avg_delta_rx_bytes,
 geo_avg_delta_rx_packets,
 geo_avg_delta_tx_bytes,
 geo_avg_delta_tx_packets,
 geo_avg_delta_io_read,
 geo_avg_delta_io_write,
 zbenchmark
}
```



# API documentation

# /monitor2

exctract a profile from a benchmark application running in a Docker container 


### HTTP Request

```
POST
```

### Request body

``` js
{
    "cid": 428e715bf771
    "bname": sysbench-cpu
}

```
### Example

##### Request

```http
http://profiler-address:1880/monitor2
```

##### Response

```http
200, Profiling of the application started succesfully
```

# /profile

exctract a profile from an application running in a Docker container and trigger the prediction operation


### HTTP Request

```
POST
```

### Request body

``` js
{
    "add": application-address
    "port": application-docker-port
    "cid":  428e715bf771
    "bname": sysbench-cpu
}

```
### Example

##### Request

```http
http://profiler-address:1880/profile
```

##### Response

```http
200, Profiling of the application started succesfully
```


# /monitorProm

exctract a profile from an application running in a container deployed in a Kubernetes Cluster


### HTTP Request

```
POST
```

### Request body

``` js
{
    "add": prometheus-address
    "port": prometheus-port
    "cid":  428e715bf771
    "bname": sysbench-cpu
}

```
### Example

##### Request

```http
http://profiler-address:1880/monitorProm
```

##### Response

```http
200, Profiling of the application started succesfully
```

# /profileProm

exctract a profile from an application running in a container deployed in a Kubernetes Cluster and trigger the prediction operation


### HTTP Request

```
POST
```

### Request body

``` js
{
    "endpoint": promethes-endpoint
    "cid": 428e715bf771
}


```
### Example

##### Request

```http
http://profiler-address:1880/profileProm
```

##### Response

```http
200, Profiling of the application started succesfully
```



[![License](https://img.shields.io/badge/License-Apache_2.0-yellowgreen.svg)](https://opensource.org/licenses/Apache-2.0)

Copyright © 2022 Institute of Communication and Computer Systems (ICCS). All rights reserved.

| ![EU Flag](http://www.consilium.europa.eu/images/img_flag-eu.gif) | This work has received funding by the European Commission under grant agreement No. 871536, Pledger project. |
|---|--------------------------------------------------------------------------------------------------------|

